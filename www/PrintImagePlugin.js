module.exports = function (imageData, filename, options, successCallback, errorCallback) {
    if (!successCallback || typeof successCallback != 'function') {
        successCallback = function () { };
    }

    if (!errorCallback || typeof errorCallback != 'function') {
        errorCallback = function (err) {
            if (err && err.trim().length > 0) console.error('projotno-cordova-plugin-print-image: ' + err);
        }
    }

    if (!imageData || imageData.trim().length == 0) {
        errorCallback('INVALID_IMAGE');
        return;
    }

    if (!filename || filename.trim().length == 0) {
        errorCallback('INVALID_FILENAME');
        return;
    }

    if (!options.positioning || ['FIT', 'FILL', 'ABSOLUTE'].indexOf(options.positioning) == -1) {
        errorCallback('INVALID_POSITIONING');
        return;
    }

    var args = [imageData, filename, options.positioning];

    if (options.positioning == 'ABSOLUTE') {
        args.push(options.x);
        args.push(options.y);
        args.push(options.width);
        args.push(options.height);
    }

    cordova.exec(successCallback, errorCallback, 'PrintImagePlugin', 'print', args);
};
